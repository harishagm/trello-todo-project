// let token = "a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975"
// let key = "0c1ab5aea68f491bbfe25d36dacf64a7"

let allCheckList = 'https://api.trello.com/1/cards/5c9a1494a9a98f36981b57a6/checklists?key=0c1ab5aea68f491bbfe25d36dacf64a7&token=a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975';
let myCheckList = 'https://api.trello.com/1/checklists/5c9a14a1248cc06a9fbac8cc/checkitems?key=0c1ab5aea68f491bbfe25d36dacf64a7&token=a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975';

let myItemlist=[];

let form = document.getElementById('addForm');
let itemList = document.getElementById('items');
let check=document.getElementsByClassName('list-group-item');

const getData = ((url) => {
    return new Promise((resolve, reject) => {
        fetch(url).then((data) => {
            return data.json();
        }).then((trelloData) => {
             resolve (trelloData);
        })

    })
})

const dataAccess = async () => {    
    myItemList = await getData(myCheckList);
    console.log(myItemList);
    if (myItemList.length > 0) {
        myItemList.forEach(items => {
            createCheckListItems(items);
        })
    }
}
dataAccess();

//---------creating the checklist items ------------//

const createCheckListItems = (ele) => {
    let itemName = ele.name;
    let li = document.createElement('li');
    li.className = "list-group-item";
    li.id = ele.id;

    let checkBox = document.createElement("input");
    checkBox.className = 'check-box'
    checkBox.setAttribute("type", "checkbox");
    if (ele.state === 'complete') {
        checkBox.checked = true;
    }

    let deleteBtn = document.createElement('button');
    deleteBtn.className = 'delete-button';
    deleteBtn.appendChild(document.createTextNode('X'));

    li.appendChild(checkBox);
    li.appendChild(document.createTextNode(itemName));
    li.appendChild(deleteBtn);
    itemList.appendChild(li);
}

//---------------add the items to the check-list------------//

function addItem(e) {
    e.preventDefault();
    let newItem = document.getElementById('item').value;
    if (newItem != "") {
        let request = new XMLHttpRequest()
        request.open('POST', "https://api.trello.com/1/checklists/5c9a14a1248cc06a9fbac8cc/checkItems?name=" + newItem + "&pos=bottom&key=0c1ab5aea68f491bbfe25d36dacf64a7&token=a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975", true)
        request.onload = function () {
            createCheckListItems(JSON.parse(this.response));
            myItemlist.push(JSON.parse(this.response));
        }
        request.send();
    }
    document.getElementById('item').value = '';
}

// -----------Remove item from the checklist---------//

function removeItem(e) {
    if (e.target.classList.contains('delete-button')) {
        var li = e.target.parentElement;
        var request = new XMLHttpRequest()
        request.open('DELETE', "https://api.trello.com/1/checklists/5c9a14a1248cc06a9fbac8cc/checkItems/" + li.id + "?key=0c1ab5aea68f491bbfe25d36dacf64a7&token=a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975", true)
        request.send();
        itemList.removeChild(li);
        myItemlist.splice(myItemList.indexOf(li.id), 1);
    }  
    
}

//-----------status of the item---------------//

function updateStatus(e) {
    if (e.target.classList.contains('check-box')) {
        var request = new XMLHttpRequest()
        var value = 'incomplete';
        //console.log(myItemlist);
        myItemlist.forEach((item) => {
            if (item.id === e.target.parentElement.id){
                if (item.state === 'incomplete') {
                    item.state = 'complete';
                    value = 'complete';
                }
                else {
                    item.state = 'incomplete';
                }
        }});
        //console.log(e.target.parentElement.id);
        request.open('PUT', "https://api.trello.com/1/cards/5c9a1494a9a98f36981b57a6/checkItem/" + e.target.parentElement.id + "?state=" + value + "&key=0c1ab5aea68f491bbfe25d36dacf64a7&token=a18af5767f2e2c3ae3c4d949723f647e1fbff15f34863ada31ee3afa401e8975", true)
        request.send();
       
    }
}

form.addEventListener('submit', addItem);
itemList.addEventListener('click', removeItem);
itemList.addEventListener('click', updateStatus);
